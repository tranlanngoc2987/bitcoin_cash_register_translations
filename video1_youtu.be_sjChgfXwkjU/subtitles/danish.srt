﻿1
00:00:00,100 --> 00:00:03,360
 Mary er en kaffebarsejer og besluttede at hun vil acceptere

2
00:00:03,360 --> 00:00:05,875
 Bitcoin Cash-betalinger for at tiltrække nye kunder.

3
00:00:05,880 --> 00:00:11,200
 Hun downloadede Bitcoin.com-tegnebogen og kom i gang med at acceptere Bitcoin Cash med det samme.

4
00:00:11,360 --> 00:00:14,180
 ”Mary er normalt i butikken, men nogle gange

5
00:00:14,180 --> 00:00:17,475
 hun er nødt til at gå væk og få det kørt af sine to assistenter,

6
00:00:17,475 --> 00:00:18,685
 Peter og Sarah.

7
00:00:19,060 --> 00:00:21,620
 Mary ville have en nem måde for Peter og Sarah

8
00:00:21,620 --> 00:00:25,120
 at fortsætte med at acceptere Bitcoin Cash-betalinger, mens hun var væk fra butikken.

9
00:00:25,615 --> 00:00:27,975
 Gå ind på Bitcoin Cash Register-appen.

10
00:00:28,415 --> 00:00:31,495
 Mary kan installere Bitcoin Cash Register App

11
00:00:31,500 --> 00:00:34,620
 direkte på Peter og Sarahs telefon eller på butikstabletten,

12
00:00:34,620 --> 00:00:37,560
 og har betalinger til butikken sendt direkte til hendes egen tegnebog.

13
00:00:37,720 --> 00:00:40,480
 Nu har Mary ro i sindet, som hun kan acceptere

14
00:00:40,480 --> 00:00:43,740
 hurtige, billige og pålidelige Bitcoin Cash-betalinger,

15
00:00:43,740 --> 00:00:45,740
 selv når hun ikke er i butikken.

16
00:00:45,740 --> 00:00:47,040
 Så hvordan gjorde Mary det?

17
00:00:47,575 --> 00:00:50,675
 For at komme i gang skal du blot gå til App Store eller Play-butikken

18
00:00:50,680 --> 00:00:52,860
 og download Bitcoin Cash Register-appen.

19
00:00:53,720 --> 00:00:55,320
 Lad os nu sætte dig op.

20
00:00:55,880 --> 00:00:59,980
 Når du har indstillet din pinkode, skal du begynde med at indtaste navnet på din butik.

21
00:01:00,500 --> 00:01:04,600
 Klik på destinationsadresse, og scan eller indsæt din Bitcoin kontantmodtagende adresse

22
00:01:05,760 --> 00:01:09,460
 Hvis du ikke har en Bitcoin Cash-tegnebog, kan du downloade Bitcoin.com-tegnebogen

23
00:01:09,460 --> 00:01:12,120
 til iOS- eller Android-enheder.

24
00:01:12,980 --> 00:01:15,440
 Til sidst skal du vælge din lokale valuta.

25
00:01:16,300 --> 00:01:17,300
 Det er det!

26
00:01:17,560 --> 00:01:18,840
 Du er klar til at gå.

27
00:01:19,165 --> 00:01:21,555
 Indtast det beløb, der skal genereres en QR-kode.

28
00:01:22,075 --> 00:01:25,025
 Dine kunder skal bare scanne og bekræfte betaling

29
00:01:25,025 --> 00:01:27,115
 og det sendes direkte til din tegnebog.

30
00:01:28,040 --> 00:01:30,480
 Da appen er beskyttet af en pinkode,

31
00:01:30,480 --> 00:01:33,220
 kun Mary kan beslutte, hvor midlerne sendes.

32
00:01:33,795 --> 00:01:36,945
 Enkelt, sikkert og hurtigere end Lyn.

33
00:01:37,755 --> 00:01:39,700
 Bitcoin Cash Register

