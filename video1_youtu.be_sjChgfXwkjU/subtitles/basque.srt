﻿1
00:00:00,100 --> 00:00:03,360
 Mary kafetegiaren jabea da eta onartu nahi duela erabaki zuen

2
00:00:03,360 --> 00:00:05,875
 Bitcoin Cash ordainketak bezero berriak erakartzeko.

3
00:00:05,880 --> 00:00:11,200
 Bitcoin.com zorroa deskargatu zuen eta berehala Bitcoin Cash onartzen hasi zen.

4
00:00:11,360 --> 00:00:14,180
 Mary dendan izaten da normalean, baina batzuetan

5
00:00:14,180 --> 00:00:17,475
 alde egin behar du eta bere bi laguntzaileek zuzendu,

6
00:00:17,475 --> 00:00:18,685
 Peter eta Sarah.

7
00:00:19,060 --> 00:00:21,620
 Mariak bide erraza nahi zuen Peter eta Sarahrentzat

8
00:00:21,620 --> 00:00:25,120
 dendatik kanpo zegoen bitartean Bitcoin Cash ordainketak onartzen jarraitu ahal izateko.

9
00:00:25,615 --> 00:00:27,975
 Sartu Bitcoin Cash Register aplikazioan.

10
00:00:28,415 --> 00:00:31,495
 Mariak Bitcoin Cash Register aplikazioa instalatu dezake

11
00:00:31,500 --> 00:00:34,620
 zuzenean Peter eta Sarah telefonoan edo dendako tabletan,

12
00:00:34,620 --> 00:00:37,560
 eta dendan ordainketak zuzenean diru-zorrora bidali.

13
00:00:37,720 --> 00:00:40,480
 Orain Mariak onar dezakeen lasaitasuna du

14
00:00:40,480 --> 00:00:43,740
 Bitcoin Cash ordainketa azkar, merkea eta fidagarria,

15
00:00:43,740 --> 00:00:45,740
 dendan ez dagoenean ere.

16
00:00:45,740 --> 00:00:47,040
 Orduan, nola egin zuen Mariak?

17
00:00:47,575 --> 00:00:50,675
 Hasteko, zoaz App Store edo Play dendara

18
00:00:50,680 --> 00:00:52,860
 eta deskargatu Bitcoin Cash Register aplikazioa.

19
00:00:53,720 --> 00:00:55,320
 Orain, dezagun konfiguratu.

20
00:00:55,880 --> 00:00:59,980
 Zure pin kodea ezarri ondoren, hasi dendaren izena idatziz.

21
00:01:00,500 --> 00:01:04,600
 Egin klik Helmuga Helbidea eta Eskaneatu edo Itsatsi zure Bitcoin Dirua jasotzeko helbidea

22
00:01:05,760 --> 00:01:09,460
 Bitcoin Cash zorroa ez baduzu, Bitcoin.com zorroa deskarga dezakezu

23
00:01:09,460 --> 00:01:12,120
 iOS edo Android gailuetarako.

24
00:01:12,980 --> 00:01:15,440
 Azkenik, hautatu Tokiko Moneta.

25
00:01:16,300 --> 00:01:17,300
 Hori da!

26
00:01:17,560 --> 00:01:18,840
 Guztiok joateko prest zaude.

27
00:01:19,165 --> 00:01:21,555
 Sartu zenbatekoa QR kode bat sortzeko.

28
00:01:22,075 --> 00:01:25,025
 Zure bezeroek ordainketa aztertu eta baieztatu besterik ez dute egin behar

29
00:01:25,025 --> 00:01:27,115
 eta zuzenean zure zorroa bidaliko da.

30
00:01:28,040 --> 00:01:30,480
 Aplikazioa kode pin batekin babestuta dagoenez,

31
00:01:30,480 --> 00:01:33,220
 Maryk bakarrik erabaki dezake dirua non bidali.

32
00:01:33,795 --> 00:01:36,945
 Tximista sinplea, segurua eta azkarragoa.

33
00:01:37,755 --> 00:01:39,700
 Bitcoin Kutxako Erregistroa

