﻿1
00:00:00,100 --> 00:00:03,360
 Mary je vlasnica kafića i odlučila je da želi prihvatiti

2
00:00:03,360 --> 00:00:05,875
 Plaćanje Bitcoin Cashom za privlačenje novih kupaca.

3
00:00:05,880 --> 00:00:11,200
 Ona je preuzela Bitcoin.com novčanik i odmah započela s prihvaćanjem Bitcoin Cash-a.

4
00:00:11,360 --> 00:00:14,180
 "Marija je obično u radnji, ali ponekad

5
00:00:14,180 --> 00:00:17,475
 ona treba da odstupi i da je vode dva pomoćnika,

6
00:00:17,475 --> 00:00:18,685
 Peter i Sarah.

7
00:00:19,060 --> 00:00:21,620
 Mary je željela lak put za Petera i Saru

8
00:00:21,620 --> 00:00:25,120
 da nastavi prihvaćati plaćanja Bitcoin Cash-a dok je bila daleko od prodavnice.

9
00:00:25,615 --> 00:00:27,975
 Uđite u aplikaciju Bitcoin blagajna.

10
00:00:28,415 --> 00:00:31,495
 Marija može instalirati aplikaciju Bitcoin Cash Registar

11
00:00:31,500 --> 00:00:34,620
 direktno na telefon Petra i Sara, ili na tablet računaru,

12
00:00:34,620 --> 00:00:37,560
 i uplate u trgovini slati direktno u njezin novčanik.

13
00:00:37,720 --> 00:00:40,480
 Sada Marija ima duševni mir koji može prihvatiti

14
00:00:40,480 --> 00:00:43,740
 brza, jeftina i pouzdana plaćanja u Bitcoin Cashu,

15
00:00:43,740 --> 00:00:45,740
 čak i kad nije kod prodavnice.

16
00:00:45,740 --> 00:00:47,040
 Pa kako je to Marija uradila?

17
00:00:47,575 --> 00:00:50,675
 Za početak jednostavno idite u App Store ili Play store

18
00:00:50,680 --> 00:00:52,860
 i preuzmite aplikaciju Bitcoin Cash Registar.

19
00:00:53,720 --> 00:00:55,320
 A sada, hajde da postavite.

20
00:00:55,880 --> 00:00:59,980
 Nakon postavljanja vašeg PIN koda, počnite sa unosom naziva svoje prodavnice.

21
00:01:00,500 --> 00:01:04,600
 Kliknite odredišnu adresu i skenirajte ili zalijepite svoju adresu za primanje Bitcoin Cash-a

22
00:01:05,760 --> 00:01:09,460
 Ako nemate Bitcoin Cash novčanik, možete preuzeti Bitcoin.com novčanik

23
00:01:09,460 --> 00:01:12,120
 za iOS ili Android uređaje.

24
00:01:12,980 --> 00:01:15,440
 Na kraju odaberite lokalnu valutu.

25
00:01:16,300 --> 00:01:17,300
 To je to!

26
00:01:17,560 --> 00:01:18,840
 Spremni ste za polazak.

27
00:01:19,165 --> 00:01:21,555
 Unesite iznos za generiranje QR koda.

28
00:01:22,075 --> 00:01:25,025
 Vaši kupci samo trebaju skenirati i potvrditi plaćanje

29
00:01:25,025 --> 00:01:27,115
 i bit će poslan direktno u vaš novčanik.

30
00:01:28,040 --> 00:01:30,480
 Budući da je aplikacija zaštićena pin-kodom,

31
00:01:30,480 --> 00:01:33,220
 samo Marija može odlučiti kamo sredstva šalju.

32
00:01:33,795 --> 00:01:36,945
 Jednostavno, sigurno i brže od munje.

33
00:01:37,755 --> 00:01:39,700
 Bitcoin blagajna

