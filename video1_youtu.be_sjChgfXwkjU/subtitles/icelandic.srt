﻿1
00:00:00,100 --> 00:00:03,360
 María er kaffihúsaeigandi og ákvað að hún vildi taka við

2
00:00:03,360 --> 00:00:05,875
 Bitcoin Cash greiðslur til að laða að nýja viðskiptavini.

3
00:00:05,880 --> 00:00:11,200
 Hún halaði niður Bitcoin.com veskinu og byrjaði strax að taka við Bitcoin Cash.

4
00:00:11,360 --> 00:00:14,180
 „María er venjulega í búðinni, en stundum

5
00:00:14,180 --> 00:00:17,475
 hún þarf að stíga frá og láta reka það af tveimur aðstoðarmönnum sínum,

6
00:00:17,475 --> 00:00:18,685
 Peter og Sarah.

7
00:00:19,060 --> 00:00:21,620
 María vildi auðvelda leið fyrir Pétur og Söru

8
00:00:21,620 --> 00:00:25,120
 til að halda áfram að taka við Bitcoin Cash greiðslum meðan hún var í burtu frá versluninni.

9
00:00:25,615 --> 00:00:27,975
 Sláðu inn Bitcoin sjóðaskráforritið.

10
00:00:28,415 --> 00:00:31,495
 Mary getur sett upp Bitcoin Cash Register App

11
00:00:31,500 --> 00:00:34,620
 beint í síma Péturs og Söru, eða á spjaldtölvu verslunarinnar,

12
00:00:34,620 --> 00:00:37,560
 og hafa greiðslur í verslunina sendar beint í eigin veski hennar.

13
00:00:37,720 --> 00:00:40,480
 Nú hefur María hugarró sem hún getur samþykkt

14
00:00:40,480 --> 00:00:43,740
 hratt, ódýrt og áreiðanlegt Bitcoin Cash-greiðslur,

15
00:00:43,740 --> 00:00:45,740
 jafnvel þegar hún er ekki í búðinni.

16
00:00:45,740 --> 00:00:47,040
 Svo hvernig gerði María það?

17
00:00:47,575 --> 00:00:50,675
 Til að byrja skaltu einfaldlega fara í App Store eða Play verslunina

18
00:00:50,680 --> 00:00:52,860
 og halaðu niður Bitcoin Cash Register App.

19
00:00:53,720 --> 00:00:55,320
 Nú skulum við koma þér upp.

20
00:00:55,880 --> 00:00:59,980
 Byrjaðu á því að slá inn nafn verslunarinnar eftir að þú hefur sett PIN númerið þitt.

21
00:01:00,500 --> 00:01:04,600
 Smelltu á áfangastaðfang og skannaðu eða límdu heimilisfang þitt fyrir Bitcoin reiðufé

22
00:01:05,760 --> 00:01:09,460
 Ef þú ert ekki með Bitcoin Cash veski geturðu halað niður Bitcoin.com veskinu

23
00:01:09,460 --> 00:01:12,120
 fyrir iOS eða Android tæki.

24
00:01:12,980 --> 00:01:15,440
 Að lokum, veldu staðbundinn gjaldmiðil þinn.

25
00:01:16,300 --> 00:01:17,300
 Það er það!

26
00:01:17,560 --> 00:01:18,840
 Þú ert allur búinn að fara.

27
00:01:19,165 --> 00:01:21,555
 Sláðu inn upphæðina til að búa til QR kóða.

28
00:01:22,075 --> 00:01:25,025
 Viðskiptavinir þínir þurfa bara að skanna og staðfesta greiðslu

29
00:01:25,025 --> 00:01:27,115
 og það verður sent beint í veskið þitt.

30
00:01:28,040 --> 00:01:30,480
 Þar sem appið er varið með pinnakóða,

31
00:01:30,480 --> 00:01:33,220
 aðeins María getur ákveðið hvert fjármunirnir eru sendir.

32
00:01:33,795 --> 00:01:36,945
 Einfalt, öruggt og fljótlegra en eldingar.

33
00:01:37,755 --> 00:01:39,700
 Bitcoin sjóðsskrá

