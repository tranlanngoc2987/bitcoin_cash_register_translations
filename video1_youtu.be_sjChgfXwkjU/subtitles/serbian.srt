﻿1
00:00:00,100 --> 00:00:03,360
 Мари је власница кафића и одлучила је да жели да прихвати

2
00:00:03,360 --> 00:00:05,875
 Плаћање Битцоин Цасх-ом за привлачење нових купаца.

3
00:00:05,880 --> 00:00:11,200
 Она је преузела Битцоин.цом новчаник и одмах започела са прихватањем Битцоин Цасх-а.

4
00:00:11,360 --> 00:00:14,180
 "Мари је обично у радњи, али понекад

5
00:00:14,180 --> 00:00:17,475
 она треба да одступи и да јој управљају два помоћника,

6
00:00:17,475 --> 00:00:18,685
 Петер и Сарах.

7
00:00:19,060 --> 00:00:21,620
 Марија је желела лак пут за Петера и Сару

8
00:00:21,620 --> 00:00:25,120
 да настави да прихвата плаћање у Битцоин Цасх-у док је била далеко од продавнице.

9
00:00:25,615 --> 00:00:27,975
 Уђите у апликацију Битцоин благајна.

10
00:00:28,415 --> 00:00:31,495
 Марија може да инсталира апликацију Битцоин Цасх Регистар

11
00:00:31,500 --> 00:00:34,620
 директно на телефон Петра и Сара, или на таблет рачунару,

12
00:00:34,620 --> 00:00:37,560
 и да уплате у продавницу шаљу директно у њен сопствени новчаник.

13
00:00:37,720 --> 00:00:40,480
 Сада Марија има душевни мир који може да прихвати

14
00:00:40,480 --> 00:00:43,740
 брза, јефтина и поуздана плаћања у Битцоин Цасху,

15
00:00:43,740 --> 00:00:45,740
 чак и кад није у продавници.

16
00:00:45,740 --> 00:00:47,040
 Па како је Марија то урадила?

17
00:00:47,575 --> 00:00:50,675
 Да бисте започели, једноставно идите у Апп Сторе или Плаи продавницу

18
00:00:50,680 --> 00:00:52,860
 и преузмите апликацију Битцоин Цасх Регистер.

19
00:00:53,720 --> 00:00:55,320
 А сада, нека вас поставе.

20
00:00:55,880 --> 00:00:59,980
 Након подешавања вашег ПИН кода, почните са уносом имена своје продавнице.

21
00:01:00,500 --> 00:01:04,600
 Кликните на Одредишну адресу и скенирајте или залепите адресу за примање Битцоин Цасх-а

22
00:01:05,760 --> 00:01:09,460
 Ако немате Битцоин Цасх новчаник, можете преузети Битцоин.цом новчаник

23
00:01:09,460 --> 00:01:12,120
 за иОС или Андроид уређаје.

24
00:01:12,980 --> 00:01:15,440
 На крају одаберите локалну валуту.

25
00:01:16,300 --> 00:01:17,300
 То је то!

26
00:01:17,560 --> 00:01:18,840
 Спремни сте да кренете.

27
00:01:19,165 --> 00:01:21,555
 Унесите износ за генерисање КР кода.

28
00:01:22,075 --> 00:01:25,025
 Ваши купци само требају скенирати и потврдити плаћање

29
00:01:25,025 --> 00:01:27,115
 и биће послат директно у ваш новчаник.

30
00:01:28,040 --> 00:01:30,480
 Будући да је апликација заштићена пин-кодом,

31
00:01:30,480 --> 00:01:33,220
 само Марија може одлучити где ће средства бити послата.

32
00:01:33,795 --> 00:01:36,945
 Једноставно, сигурно и брже од стреле.

33
00:01:37,755 --> 00:01:39,700
 Битцоин благајна

