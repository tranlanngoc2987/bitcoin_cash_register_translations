﻿1
00:00:00,100 --> 00:00:03,360
Maria possiede un bar e ha deciso di accettare

2
00:00:03,360 --> 00:00:05,875
pagamenti in Bitcoin Cash per aumentare la sua clientela.

3
00:00:05,880 --> 00:00:11,200
Ha scaricato il portafoglio Bitcoin.com e ha subito iniziato ad accettare Bitcoin Cash.

4
00:00:11,360 --> 00:00:14,180
Di solito Maria è al negozio, ma a volte

5
00:00:14,180 --> 00:00:17,475
deve allontanarsi e farlo gestire dai suoi due assistenti,

6
00:00:17,475 --> 00:00:18,685
Pietro e Sara.

7
00:00:19,060 --> 00:00:21,620
Maria vuole un modo semplice per Pietro e Sara

8
00:00:21,620 --> 00:00:25,120
per continuare ad accettare pagamenti con Bitcoin Cash anche quando è lontana dal negozio.

9
00:00:25,615 --> 00:00:27,975
Accedi all'applicazione Bitcoin Cash Register.

10
00:00:28,415 --> 00:00:31,495
Maria può installare l'applicazione Bitcoin Cash Register

11
00:00:31,500 --> 00:00:34,620
direttamente sul telefono di Pietro e Sara o sul tablet del negozio,

12
00:00:34,620 --> 00:00:37,560
e far trasferire i pagamenti dal negozio direttamente al suo portafoglio.

13
00:00:37,720 --> 00:00:40,480
In questo modo Maria può serenamente accettare

14
00:00:40,480 --> 00:00:43,740
pagamenti in Bitcoin Cash veloci, economici e affidabili,

15
00:00:43,740 --> 00:00:45,740
anche quando non è in negozio.

16
00:00:45,740 --> 00:00:47,040
Quindi come ha fatto Maria?

17
00:00:47,575 --> 00:00:50,675
Per iniziare, vai all'App Store o al Play Store.

18
00:00:50,680 --> 00:00:52,860
e scarica l'applicazione Bitcoin Cash Register.

19
00:00:53,720 --> 00:00:55,320
Accedi al menu di configurazione.

20
00:00:55,880 --> 00:00:59,980
Dopo aver definito il tuo codice PIN, inserisci il nome del tuo negozio.

21
00:01:00,500 --> 00:01:04,600
Fai clic su Indirizzo di Destinazione e Scansiona o incolla il tuo Indirizzo di Ricezione di Bitcoin Cash

22
00:01:05,760 --> 00:01:09,460
Se non hai un portafoglio Bitcoin Cash, puoi scaricare il portafoglio Bitcoin.com Wallet

23
00:01:09,460 --> 00:01:12,120
per dispositivi IOS o Android.

24
00:01:12,980 --> 00:01:15,440
Infine, scegli la tua valuta locale.

25
00:01:16,300 --> 00:01:17,300
Questo è tutto!

26
00:01:17,560 --> 00:01:18,840
Sei pronto!

27
00:01:19,165 --> 00:01:21,555
Inserisci l'importo per generare un codice QR.

28
00:01:22,075 --> 00:01:25,025
I tuoi clienti devono solo scansionare e confermare il pagamento

29
00:01:25,025 --> 00:01:27,115
e verrà trasferito direttamente sul tuo portafoglio.

30
00:01:28,040 --> 00:01:30,480
Poichè l'applicazione è protetta da un codice PIN,

31
00:01:30,480 --> 00:01:33,220
solo Maria può decidere dove verranno inviati i fondi.

32
00:01:33,795 --> 00:01:36,945
Semplice, sicuro e più veloce di un lampo.

33
00:01:37,755 --> 00:01:39,700
Bitcoin Cash Register

