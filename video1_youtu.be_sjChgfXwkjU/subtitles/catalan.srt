﻿1
00:00:00,100 --> 00:00:03,360
 Mary és propietària d’una cafeteria i va decidir que vol acceptar

2
00:00:03,360 --> 00:00:05,875
 Pagaments en efectiu de Bitcoin per atraure nous clients.

3
00:00:05,880 --> 00:00:11,200
 Ella va descarregar la cartera de Bitcoin.com i va començar a acceptar Bitcoin Cash immediatament.

4
00:00:11,360 --> 00:00:14,180
 "Maria sol estar a la botiga, però de vegades

5
00:00:14,180 --> 00:00:17,475
 necessita abandonar-la i fer-la passar pels dos ajudants,

6
00:00:17,475 --> 00:00:18,685
 Peter i Sarah.

7
00:00:19,060 --> 00:00:21,620
 Maria volia un camí fàcil per a Peter i Sarah

8
00:00:21,620 --> 00:00:25,120
 per continuar acceptant els pagaments de Bitcoin Cash mentre ella estava lluny de la botiga.

9
00:00:25,615 --> 00:00:27,975
 Entra a l'aplicació de registre de caixa Bitcoin.

10
00:00:28,415 --> 00:00:31,495
 Maria pot instal·lar l’aplicació Bitcoin Cash Register

11
00:00:31,500 --> 00:00:34,620
 directament al telèfon de Peter i Sarah o a la tauleta de la botiga,

12
00:00:34,620 --> 00:00:37,560
 i enviar els pagaments a la botiga directament a la seva pròpia cartera.

13
00:00:37,720 --> 00:00:40,480
 Ara Maria té tranquil·litat que pot acceptar

14
00:00:40,480 --> 00:00:43,740
 Pagaments en efectiu ràpids, econòmics i econòmics,

15
00:00:43,740 --> 00:00:45,740
 fins i tot quan no és a la botiga.

16
00:00:45,740 --> 00:00:47,040
 Com ho va fer Maria?

17
00:00:47,575 --> 00:00:50,675
 Per començar, només cal que vagi a l'App Store o Play Store

18
00:00:50,680 --> 00:00:52,860
 i descarregueu l'aplicació Bitcoin Cash Register.

19
00:00:53,720 --> 00:00:55,320
 Ara, anem a configurar.

20
00:00:55,880 --> 00:00:59,980
 Després d'haver definit el codi de fixació, comenceu per introduir el nom de la botiga.

21
00:01:00,500 --> 00:01:04,600
 Feu clic a Adreça de destinació i escanegeu o enganxeu la vostra adreça de recepció de diners en efectiu Bitcoin

22
00:01:05,760 --> 00:01:09,460
 Si no teniu una cartera Bitcoin Cash, podeu descarregar la carteira de Bitcoin.com

23
00:01:09,460 --> 00:01:12,120
 per a dispositius iOS o Android.

24
00:01:12,980 --> 00:01:15,440
 Finalment, seleccioneu la vostra moneda local.

25
00:01:16,300 --> 00:01:17,300
 Això és!

26
00:01:17,560 --> 00:01:18,840
 Esteu tots preparats.

27
00:01:19,165 --> 00:01:21,555
 Introduïu l’import per generar un codi QR.

28
00:01:22,075 --> 00:01:25,025
 Els vostres clients només necessiten escanejar i confirmar el pagament

29
00:01:25,025 --> 00:01:27,115
 i s’enviarà directament a la cartera.

30
00:01:28,040 --> 00:01:30,480
 Com que l’aplicació està protegida per un codi pin,

31
00:01:30,480 --> 00:01:33,220
 només Maria pot decidir on s’envien els fons.

32
00:01:33,795 --> 00:01:36,945
 Senzill, segur i ràpid que el llamp

33
00:01:37,755 --> 00:01:39,700
 Registre de caixa Bitcoin

