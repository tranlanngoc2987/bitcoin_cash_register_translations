﻿1
00:00:00,100 --> 00:00:03,360
 Marija yra kavinės savininkė ir nusprendė, kad nori sutikti

2
00:00:03,360 --> 00:00:05,875
 „Bitcoin Cash“ mokėjimai, siekiant pritraukti naujų klientų.

3
00:00:05,880 --> 00:00:11,200
 Ji atsisiuntė „Bitcoin.com“ piniginę ir iškart pradėjo priimti „Bitcoin Cash“.

4
00:00:11,360 --> 00:00:14,180
 „Marija dažniausiai būna parduotuvėje, bet kartais

5
00:00:14,180 --> 00:00:17,475
 jai reikia pasitraukti ir leisti jai vadovauti dviem savo padėjėjoms,

6
00:00:17,475 --> 00:00:18,685
 Petras ir Sara.

7
00:00:19,060 --> 00:00:21,620
 Marija norėjo Petro ir Saros lengvo kelio

8
00:00:21,620 --> 00:00:25,120
 toliau priimti „Bitcoin Cash“ mokėjimus, kol ji nebuvo parduotuvėje.

9
00:00:25,615 --> 00:00:27,975
 Įveskite „Bitcoin“ kasos programos programą.

10
00:00:28,415 --> 00:00:31,495
 Marija gali įdiegti „Bitcoin“ kasos programos programą

11
00:00:31,500 --> 00:00:34,620
 tiesiogiai Petro ir Saros telefone arba parduotuvės planšetiniame kompiuteryje,

12
00:00:34,620 --> 00:00:37,560
 ir mokėjimus į parduotuvę nusiųskite tiesiai į jos pačios piniginę.

13
00:00:37,720 --> 00:00:40,480
 Dabar Marijai ramu, kurią ji gali priimti

14
00:00:40,480 --> 00:00:43,740
 greiti, pigūs ir patikimi „Bitcoin Cash“ mokėjimai,

15
00:00:43,740 --> 00:00:45,740
 net tada, kai jos nėra parduotuvėje.

16
00:00:45,740 --> 00:00:47,040
 Taigi kaip Marija tai padarė?

17
00:00:47,575 --> 00:00:50,675
 Norėdami pradėti, tiesiog eikite į „App Store“ arba „Play“ parduotuvę

18
00:00:50,680 --> 00:00:52,860
 ir atsisiųskite „Bitcoin“ kasos programos programą.

19
00:00:53,720 --> 00:00:55,320
 Dabar leiskime jums nustatyti.

20
00:00:55,880 --> 00:00:59,980
 Nustatę savo PIN kodą, pirmiausia įveskite savo parduotuvės pavadinimą.

21
00:01:00,500 --> 00:01:04,600
 Spustelėkite Paskirties adresas ir nuskaitykite arba įklijuokite savo „Bitcoin Cash“ gavimo adresą

22
00:01:05,760 --> 00:01:09,460
 Jei neturite „Bitcoin Cash“ piniginės, galite atsisiųsti „Bitcoin.com“ piniginę

23
00:01:09,460 --> 00:01:12,120
 „iOS“ ar „Android“ įrenginiams.

24
00:01:12,980 --> 00:01:15,440
 Galiausiai pasirinkite savo vietinę valiutą.

25
00:01:16,300 --> 00:01:17,300
 Viskas!

26
00:01:17,560 --> 00:01:18,840
 Jūs visi pasiruošę eiti.

27
00:01:19,165 --> 00:01:21,555
 Įveskite sumą, kad gautumėte QR kodą.

28
00:01:22,075 --> 00:01:25,025
 Jūsų klientams tereikia nuskaityti ir patvirtinti mokėjimą

29
00:01:25,025 --> 00:01:27,115
 ir jis bus išsiųstas tiesiai į jūsų piniginę.

30
00:01:28,040 --> 00:01:30,480
 Kadangi programa yra apsaugota PIN kodu,

31
00:01:30,480 --> 00:01:33,220
 tik Marija gali nuspręsti, kur siunčiamos lėšos.

32
00:01:33,795 --> 00:01:36,945
 Paprastas, saugus ir greitesnis nei žaibas.

33
00:01:37,755 --> 00:01:39,700
 „Bitcoin“ kasos aparatas

