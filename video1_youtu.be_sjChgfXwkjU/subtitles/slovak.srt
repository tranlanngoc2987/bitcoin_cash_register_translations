﻿1
00:00:00,100 --> 00:00:03,360
 Mary je majiteľkou kaviarne a rozhodla sa, že ju chce prijať

2
00:00:03,360 --> 00:00:05,875
 Bitcoinové hotovostné platby na prilákanie nových zákazníkov.

3
00:00:05,880 --> 00:00:11,200
 Stiahla si peňaženku Bitcoin.com a ihneď začala prijímať hotovostné bitcoiny.

4
00:00:11,360 --> 00:00:14,180
 „Mary je zvyčajne v obchode, ale niekedy

5
00:00:14,180 --> 00:00:17,475
 musí ustúpiť a nechať ju viesť jej dvaja asistenti,

6
00:00:17,475 --> 00:00:18,685
 Peter a Sarah.

7
00:00:19,060 --> 00:00:21,620
 Mary chcela pre Petra a Sarah jednoduchý spôsob

8
00:00:21,620 --> 00:00:25,120
 aby pokračovala v prijímaní platieb v bitcoínovej hotovosti, keď bola mimo obchodu.

9
00:00:25,615 --> 00:00:27,975
 Vstúpte do aplikácie Bitcoin Cash Register App.

10
00:00:28,415 --> 00:00:31,495
 Mary si môže nainštalovať aplikáciu Bitcoin Cash Register App

11
00:00:31,500 --> 00:00:34,620
 priamo na telefóne Petra a Sarah alebo na tablete obchodu,

12
00:00:34,620 --> 00:00:37,560
 a nechať si platby do obchodu poslať priamo na svoju vlastnú peňaženku.

13
00:00:37,720 --> 00:00:40,480
 Teraz má Mária pokoj, ktorý dokáže prijať

14
00:00:40,480 --> 00:00:43,740
 rýchle, lacné a spoľahlivé platby bitcoinmi v hotovosti,

15
00:00:43,740 --> 00:00:45,740
 aj keď nie je v obchode.

16
00:00:45,740 --> 00:00:47,040
 Ako to urobila Mary?

17
00:00:47,575 --> 00:00:50,675
 Ak chcete začať, jednoducho choďte do obchodu App Store alebo Play Play

18
00:00:50,680 --> 00:00:52,860
 a stiahnite si aplikáciu Bitcoin Cash Register.

19
00:00:53,720 --> 00:00:55,320
 Teraz vás pripravíme.

20
00:00:55,880 --> 00:00:59,980
 Po nastavení kódu PIN začnite zadaním názvu obchodu.

21
00:01:00,500 --> 00:01:04,600
 Kliknite na cieľovú adresu a naskenujte alebo prilepte adresu na príjem hotovosti v bitcoinoch

22
00:01:05,760 --> 00:01:09,460
 Ak nemáte hotovostnú peňaženku Bitcoin, môžete si stiahnuť peňaženku Bitcoin.com

23
00:01:09,460 --> 00:01:12,120
 pre zariadenia so systémom iOS alebo Android.

24
00:01:12,980 --> 00:01:15,440
 Nakoniec vyberte svoju miestnu menu.

25
00:01:16,300 --> 00:01:17,300
 To je všetko!

26
00:01:17,560 --> 00:01:18,840
 Ste pripravení ísť.

27
00:01:19,165 --> 00:01:21,555
 Zadajte sumu na vygenerovanie QR kódu.

28
00:01:22,075 --> 00:01:25,025
 Vaši zákazníci jednoducho potrebujú skenovať a potvrdiť platbu

29
00:01:25,025 --> 00:01:27,115
 a odošle sa priamo do vašej peňaženky.

30
00:01:28,040 --> 00:01:30,480
 Keďže je aplikácia chránená kódom PIN,

31
00:01:30,480 --> 00:01:33,220
 iba Mária môže rozhodnúť, kam sa prostriedky pošlú.

32
00:01:33,795 --> 00:01:36,945
 Jednoduché, bezpečné a rýchlejšie ako blesk.

33
00:01:37,755 --> 00:01:39,700
 Pokladnica bitcoinových hotovosti

