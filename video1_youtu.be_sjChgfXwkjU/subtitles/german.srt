﻿1
00:00:00,100 --> 00:00:03,360
Mary ist Inhaberin eines Cafés und hat beschlossen,

2
00:00:03,360 --> 00:00:05,875
dass sie als Zahlungsmittel Bitcoin Cash
zur Gewinnung neuer Kunden akzeptieren möchte.

3
00:00:05,880 --> 00:00:11,200
Sie lädt die Wallet-App von Bitcoin.com herunter und beginnt sofort,
Bitcoin Cash zu akzeptieren.

4
00:00:11,360 --> 00:00:14,180
Mary ist normalerweise im Laden, aber manchmal

5
00:00:14,180 --> 00:00:17,475
muss sie weg und das Geschäft ihren beiden Assistenten,

6
00:00:17,475 --> 00:00:18,685
Peter und Sarah, überlassen.

7
00:00:19,060 --> 00:00:21,620
Mary suchte eine einfache Möglichkeit, wie Peter und Sarah

8
00:00:21,620 --> 00:00:25,120
weiterhin Bitcoin Cash akzeptieren können, während sie nicht im Geschäft ist.

9
00:00:25,615 --> 00:00:27,975
Wir stellen vor: die Bitcoin Cash Register App.

10
00:00:28,415 --> 00:00:31,495
Mary installiert die Bitcoin Cash Register App

11
00:00:31,500 --> 00:00:34,620
auf Peter und Sarah's Geschäftsmobiltelefonen oder auf dem Geschäfts-Tablet,

12
00:00:34,620 --> 00:00:37,560
und Zahlungen im Café werden direkt an ihr eigenes Wallet geschickt.

13
00:00:37,720 --> 00:00:40,480
Jetzt kann Mary seelenruhig schnelle, günstige und zuverlässige

14
00:00:40,480 --> 00:00:43,740
Zahlungen in Bitcoin Cash akzeptieren,

15
00:00:43,740 --> 00:00:45,740
auch wenn sie nicht im Laden ist.

16
00:00:45,740 --> 00:00:47,040
Wie genau machen Sie das?

17
00:00:47,575 --> 00:00:50,675
Rufen Sie einfach den App Store oder Play Store auf, um loszulegen

18
00:00:50,680 --> 00:00:52,860
und laden Sie die Bitcoin Cash Register App herunter.

19
00:00:53,720 --> 00:00:55,320
Nun machen wir fix die restlichen Einstellungen.

20
00:00:55,880 --> 00:00:59,980
Nachdem Sie Ihren PIN-Code eingegeben haben, tragen Sie zunächst den Namen Ihres Geschäfts ein.

21
00:01:00,500 --> 00:01:04,600
Klicken Sie auf Zieladresse und scannen oder fügen Sie Ihre Bitcoin Cash Empfangsadresse ein.

22
00:01:05,760 --> 00:01:09,460
Wenn Sie noch kein Bitcoin Cash Wallet haben, können Sie die Bitcoin.com Wallet App

23
00:01:09,460 --> 00:01:12,120
für iOS oder Android herunterladen.

24
00:01:12,980 --> 00:01:15,440
Schließlich wählen Sie Ihre lokale Währung.

25
00:01:16,300 --> 00:01:17,300
Das war's schon!

26
00:01:17,560 --> 00:01:18,840
Sie sind startklar.

27
00:01:19,165 --> 00:01:21,555
Geben Sie den Betrag ein, um einen QR-Code zu generieren.

28
00:01:22,075 --> 00:01:25,025
Ihre Kunden müssen nur einscannen und die Zahlung bestätigen

29
00:01:25,025 --> 00:01:27,115
und das Geld wird direkt an Ihr Wallet verschickt.

30
00:01:28,040 --> 00:01:30,480
Da die App durch einen PIN-Code geschützt ist,

31
00:01:30,480 --> 00:01:33,220
können nur Sie entscheiden, wohin die Zahlungen geschickt werden.

32
00:01:33,795 --> 00:01:36,945
Einfach, sicher und blitzschnell.

33
00:01:37,755 --> 00:01:39,700
Bitcoin Cash Register

