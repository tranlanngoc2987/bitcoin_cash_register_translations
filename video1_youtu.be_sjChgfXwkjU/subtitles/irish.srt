﻿1
00:00:00,100 --> 00:00:03,360
 Is úinéir siopa caife í Mary agus shocraigh sí gur mian léi glacadh leis

2
00:00:03,360 --> 00:00:05,875
 Íocaíochtaí Bitcoin Airgid chun custaiméirí nua a mhealladh.

3
00:00:05,880 --> 00:00:11,200
 Íoslódáil sí an sparán Bitcoin.com agus thosaigh sí ag glacadh le Bitcoin Cash láithreach.

4
00:00:11,360 --> 00:00:14,180
 "Is iondúil go bhfuil Mary sa siopa, ach uaireanta

5
00:00:14,180 --> 00:00:17,475
 caithfidh sí céim ar shiúl agus í a reáchtáil ag a beirt chúntóirí,

6
00:00:17,475 --> 00:00:18,685
 Peadar agus Sarah.

7
00:00:19,060 --> 00:00:21,620
 Theastaigh ó Mháire bealach éasca do Pheadar agus do Sarah

8
00:00:21,620 --> 00:00:25,120
 leanúint le glacadh le híocaíochtaí Bitcoin Cash nuair a bhí sí ar shiúl ón siopa.

9
00:00:25,615 --> 00:00:27,975
 Cuir isteach an Bitcoin Cash Register App.

10
00:00:28,415 --> 00:00:31,495
 Is féidir le Máire App App Register Bitcoin a shuiteáil

11
00:00:31,500 --> 00:00:34,620
 go díreach ar ghuthán Peter agus Sarah, nó ar an táibléad siopa,

12
00:00:34,620 --> 00:00:37,560
 agus seolfar íocaíochtaí chuig an siopa go díreach chuig a sparán féin.

13
00:00:37,720 --> 00:00:40,480
 Anois tá suaimhneas intinne ag Máire gur féidir léi glacadh leis

14
00:00:40,480 --> 00:00:43,740
 íocaíochtaí Bitcoin, tapa, saor agus iontaofa,

15
00:00:43,740 --> 00:00:45,740
 fiú nuair nach bhfuil sí sa siopa.

16
00:00:45,740 --> 00:00:47,040
 Mar sin, conas a rinne Máire é?

17
00:00:47,575 --> 00:00:50,675
 Chun tús a chur leis, téigh go dtí an App Store nó an siopa súgartha

18
00:00:50,680 --> 00:00:52,860
 agus íoslódáil an App Bitcoin Cash Register.

19
00:00:53,720 --> 00:00:55,320
 Anois, a ligean dúinn a chur ar bun.

20
00:00:55,880 --> 00:00:59,980
 Tar éis do chód bioráin a shocrú, cuir tús le hainm do stóir.

21
00:01:00,500 --> 00:01:04,600
 Cliceáil Seoladh an Cheann Scríbe agus Scanáil nó Greamaigh do sheoladh Bitcoin Cash Cash

22
00:01:05,760 --> 00:01:09,460
 Mura bhfuil sparán Bitcoin Cash agat, is féidir leat an Sparán Bitcoin.com a íoslódáil

23
00:01:09,460 --> 00:01:12,120
 le haghaidh feistí iOS nó Android.

24
00:01:12,980 --> 00:01:15,440
 Ar deireadh, roghnaigh do Airgeadra Áitiúil.

25
00:01:16,300 --> 00:01:17,300
 Sin é!

26
00:01:17,560 --> 00:01:18,840
 Tá tú ar tí dul ar aghaidh.

27
00:01:19,165 --> 00:01:21,555
 Cuir isteach an méid chun cód QR a ghiniúint.

28
00:01:22,075 --> 00:01:25,025
 Ní gá do chustaiméirí ach íocaíocht a scanadh agus a dheimhniú

29
00:01:25,025 --> 00:01:27,115
 agus seolfar é go díreach chuig do sparán.

30
00:01:28,040 --> 00:01:30,480
 Ós rud é go bhfuil an feidhmchlár cosanta ag cód bioráin,

31
00:01:30,480 --> 00:01:33,220
 ní féidir ach le Mary cinneadh a dhéanamh i dtaobh cá seoltar na cistí.

32
00:01:33,795 --> 00:01:36,945
 Simplí, slán, agus níos tapúla ná tintreach.

33
00:01:37,755 --> 00:01:39,700
 Clár Airgid Bitcoin

