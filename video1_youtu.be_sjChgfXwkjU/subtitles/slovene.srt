﻿1
00:00:00,100 --> 00:00:03,360
Mary je lastnica kavarne in odločila se je,
da bo sprejemala plačila v valuti

2
00:00:03,360 --> 00:00:05,875
Bitcoin Cash, saj želi privabiti nove stranke.

3
00:00:05,880 --> 00:00:11,200
Prenesla je denarnico Bitcoin.com Wallet in takoj začela sprejemati valuto Bitcoin Cash.

4
00:00:11,360 --> 00:00:14,180
Običajno v kavarni dela Mary, toda občasno

5
00:00:14,180 --> 00:00:17,475
mora po opravkih in takrat
kavarno vodita njena pomočnika,

6
00:00:17,475 --> 00:00:18,685
Peter in Sarah.

7
00:00:19,060 --> 00:00:21,620
Mary je želela Petru in Sarah

8
00:00:21,620 --> 00:00:25,120
olajšati sprejemanje plačil v valuti Bitcoin Cash, kadar nje ni v kavarni.

9
00:00:25,615 --> 00:00:27,975
In tu nastopi aplikacija Bitcoin Cash Register.

10
00:00:28,415 --> 00:00:31,495
Mary lahko aplikacijo Bitcoin Cash Register namesti

11
00:00:31,500 --> 00:00:34,620
neposredno v telefon od Petra in Sarah ali v skupni tablični računalnik,

12
00:00:34,620 --> 00:00:37,560
plačila v kavarni pa se nato pošiljajo neposredno v njeno denarnico.

13
00:00:37,720 --> 00:00:40,480
Zdaj je Mary pomirjena, saj lahko plačila v valuti Bitcoin Cash

14
00:00:40,480 --> 00:00:43,740
sprejema hitro, ugodno in zanesljivo,

15
00:00:43,740 --> 00:00:45,740
tudi če ni prisotna v kavarni.

16
00:00:45,740 --> 00:00:47,040
Kako je Mary to uspelo?

17
00:00:47,575 --> 00:00:50,675
Za začetek preprosto odprite trgovino App Store ali Play

18
00:00:50,680 --> 00:00:52,860
in prenesite aplikacijo Bitcoin Cash Register.

19
00:00:53,720 --> 00:00:55,320
Nato sledi nastavitev.

20
00:00:55,880 --> 00:00:59,980
Ko nastavite kodo PIN, vnesite ime kavarne.

21
00:01:00,500 --> 00:01:04,600
Kliknite Destination Address in optično preberite ali prilepite naslov za prejemanje plačil v valuti Bitcoin Cash.

22
00:01:05,760 --> 00:01:09,460
Če nimate denarnice Bitcoin Cash wallet, lahko prenesete denarnico Bitcoin.com Wallet

23
00:01:09,460 --> 00:01:12,120
za naprave iOS ali Android.

24
00:01:12,980 --> 00:01:15,440
Na koncu izberite še lokalno valuto.

25
00:01:16,300 --> 00:01:17,300
In to je to!

26
00:01:17,560 --> 00:01:18,840
Pripravljeni ste na uporabo.

27
00:01:19,165 --> 00:01:21,555
Vnesite znesek, da ustvarite kodo QR.

28
00:01:22,075 --> 00:01:25,025
Stranke morajo nato zgolj optično prebrati kodo in potrditi plačilo,

29
00:01:25,025 --> 00:01:27,115
ki bo poslano neposredno v vašo denarnico.

30
00:01:28,040 --> 00:01:30,480
Ker je aplikacija zaščitena s kodo PIN,

31
00:01:30,480 --> 00:01:33,220
lahko samo Mary odloči, kam bodo sredstva poslana.

32
00:01:33,795 --> 00:01:36,945
Preprosto, varno in bliskovito hitro.

33
00:01:37,755 --> 00:01:39,700
Bitcoin Cash Register
