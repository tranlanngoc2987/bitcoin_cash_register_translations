#!/bin/bash
# trim leading whitespace from file.
# creates a .trimmed file

sed 's/^[[:space:]]*//' "$1" > "$1.trimmed"
